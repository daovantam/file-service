package com.mor.fileservice.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.mor.fileservice.dto.UploadFileToS3Dto;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UploadService {

  private final AmazonS3 amazonS3;

  @Autowired
  public UploadService(AmazonS3 amazonS3) {
    this.amazonS3 = amazonS3;
  }

  public String uploadToS3(UploadFileToS3Dto uploadFileToS3Dto) throws IOException {
    File file = new File(uploadFileToS3Dto.getFileName());
    OutputStream outputStream = new FileOutputStream(file);
    outputStream.write(uploadFileToS3Dto.getFile());

    String bucketName = "house-rental-media";
    PutObjectRequest request = new PutObjectRequest(bucketName,
        uploadFileToS3Dto.getFolderName() + "/" + file.getName(), file)
        .withCannedAcl(CannedAccessControlList.PublicRead);
    amazonS3.putObject(request);
    
    log.info("upload file with name {} done.", file.getName());

    return amazonS3.getUrl(bucketName, uploadFileToS3Dto.getFolderName() + "/" + file.getName())
        .toString();
  }
}
