package com.mor.fileservice.factory;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class GeneralResponse<T> implements Serializable {

  @JsonProperty("Time")
  @JsonFormat(shape = Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss.SSS", timezone = "Asia/Ho_Chi_Minh")
  private Date timestamp;

  @JsonProperty("status")
  private ResponseStatus status;

  @JsonProperty("data")
  private T data;
}