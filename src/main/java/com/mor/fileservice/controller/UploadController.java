package com.mor.fileservice.controller;

import com.mor.fileservice.dto.UploadFileToS3Dto;
import com.mor.fileservice.factory.ResponseFactory;
import com.mor.fileservice.service.UploadService;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/upload")
public class UploadController {

  private final UploadService uploadService;

  private final ResponseFactory responseFactory;

  @Autowired
  public UploadController(UploadService uploadService, ResponseFactory responseFactory) {
    this.uploadService = uploadService;
    this.responseFactory = responseFactory;
  }

  @PostMapping
  public ResponseEntity<?> upload(@RequestBody UploadFileToS3Dto uploadFileToS3Dto)
      throws IOException {
    String putObjectResult = uploadService.uploadToS3(uploadFileToS3Dto);

    return responseFactory.created(putObjectResult, String.class);
  }
}
