package com.mor.fileservice.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResponseStatusCodeEnum {
  SUCCESS("MNG200", "Success"),
  CREATED("MNG201", "Created"),
  NO_PERMISSION("MNG40000", "The client has no permission calling this API"),
  INVALID_PARAMETER("MNG40001", "Invalid request parameter"),
  INTERNAL_SERVER_ERROR("MNG50000", "Internal server error"),
  SCOPE_NOT_FOUND("MNG40004", "Scope not found"),
  NO_DATA_CHANGE("MNG40005", "No data has been change"),
  RESOURCE_NOT_FOUND("MNG4040", "Resource not found"),
  FILE_TYPE_INCORRECT("MNG4041", "File type incorrect"),
  CODE_INVALID("MNG40002", "Code has already exists, please select another code");
  private final String code;
  private final String message;
}
