package com.mor.fileservice.dto;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UploadFileToS3Dto implements Serializable {

  private String folderName;

  private String fileName;

  private byte[] file;
}
