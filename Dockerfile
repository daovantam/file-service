FROM openjdk:8
ADD target/file-service.jar file-service.jar
EXPOSE 8087
RUN mkdir /images
ENTRYPOINT ["java", "-jar", "file-service.jar"]